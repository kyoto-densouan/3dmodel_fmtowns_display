# README #

1/3スケールの富士通 FM TOWNS用ディスプレイ風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fmtowns_display/raw/d78909794fd2dc95401a61c1db1a4707821dbaa2/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fmtowns_display/raw/d78909794fd2dc95401a61c1db1a4707821dbaa2/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fmtowns_display/raw/d78909794fd2dc95401a61c1db1a4707821dbaa2/ExampleImage.png)
